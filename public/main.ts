import { bootstrap }    from '@angular/platform-browser-dynamic';
import { PlanetMapperComponent } from './components/planet-mapper/planetMapper';
import './rxjs/rxjs';

bootstrap(PlanetMapperComponent);