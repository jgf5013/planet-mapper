"use strict";
var Planet = (function () {
    function Planet() {
    }
    Planet.prototype.getDiscoveryDate = function () {
        if (this.pl_publ_date) {
            return new Date(this.pl_publ_date + '-01');
        }
        else {
            return new Date(this.pl_disc + '-01-01');
        }
    };
    return Planet;
}());
exports.Planet = Planet;
//# sourceMappingURL=planet.js.map