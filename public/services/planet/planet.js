"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
console.log('HTTP_PROVIDERS: ', http_1.HTTP_PROVIDERS);
console.log('Http: ', http_1.Http);
var PlanetService = (function () {
    function PlanetService(http) {
        this.http = http;
        this.BASE_QUERY_API = 'http://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?table=exoplanets&format=json';
        console.log('PlanetService created.', http);
        this.queryApi = this.BASE_QUERY_API + '&select=pl_hostname,st_spstr,st_metfe,st_age,pl_bmassprov,pl_rade,pl_dens,st_teff,pl_pnum,pl_name,pl_disc,pl_telescope,pl_pelink,pl_bmasse,pl_publ_date&order=pl_hostname';
    }
    PlanetService.prototype.getPlanets = function () {
        console.log('you called PlanetService!');
        return this.http.get(this.queryApi)
            .map(function (response) { return response.json(); });
    };
    PlanetService = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Inject(http_1.Http)), 
        __metadata('design:paramtypes', [http_1.Http])
    ], PlanetService);
    return PlanetService;
}());
exports.PlanetService = PlanetService;
//# sourceMappingURL=planet.js.map