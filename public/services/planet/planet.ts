import { Component, Injectable, Inject } from '@angular/core';
import { HTTP_PROVIDERS, Http } from '@angular/http';
import { Planet } from '../../dto/planet/planet';
import { Observable } from 'rxjs/Observable';

console.log('HTTP_PROVIDERS: ', HTTP_PROVIDERS);
console.log('Http: ', Http);

@Injectable()
export class PlanetService {

  BASE_QUERY_API: string = 'http://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?table=exoplanets&format=json';
  queryApi: string;
  
  constructor(@Inject(Http) private http: Http) {
    console.log('PlanetService created.', http);
    this.queryApi = this.BASE_QUERY_API + '&select=pl_hostname,st_spstr,st_metfe,st_age,pl_bmassprov,pl_rade,pl_dens,st_teff,pl_pnum,pl_name,pl_disc,pl_telescope,pl_pelink,pl_bmasse,pl_publ_date&order=pl_hostname';
  }


  getPlanets() : Observable<Planet[]> {


    console.log('you called PlanetService!');

    return this.http.get(this.queryApi)
      .map(response => response.json());
  }

  // private _transformApiToPlanet(response: Observable<any[]>): Observable<Planet[]> {

  //   response.json().map(apiPlanet => {
  //     return {

  //     }
  //   })
  // }

}