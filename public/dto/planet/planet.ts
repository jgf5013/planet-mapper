export class Planet {
    pl_hostname: string;
    st_spstr: string;
    st_metfe: number;
    st_age: number;
    pl_bmassprov: string;
    pl_rade: number; //radius in units of earth-units
    pl_dens: number;
    st_teff: number;
    pl_pnum: number;
    pl_name: string;
    pl_disc: number;
    pl_telescope: string;
    pl_pelink: string;
    pl_bmasse: number;
    pl_publ_date: string;

    constructor() {

    }

    getDiscoveryDate(): Date {
        if(this.pl_publ_date) {
            return new Date(this.pl_publ_date + '-01');   
        } else {
            return new Date(this.pl_disc + '-01-01');
        }
    }
}
