"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var d3 = require('d3');
var planet_1 = require('../../services/planet/planet');
var PlanetMapperComponent = (function () {
    function PlanetMapperComponent(planetService) {
        this.planetService = planetService;
        this.planets = [];
        this.diameter = 960;
        this.format = d3.format(",d");
        this.color = d3.scale.category20c();
    }
    PlanetMapperComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.svgContainer = d3.select("#planetMapperSvg")
            .append("svg")
            .attr("width", 10000)
            .attr("height", 10000);
        console.log('PlanetMapperComponent.constructor...');
        this.planetService.getPlanets()
            .subscribe(function (planets) {
            _this._populateSvgContainer(planets);
        });
    };
    PlanetMapperComponent.prototype._populateSvgContainer = function (planets) {
        var _this = this;
        console.log('planets: ', planets);
        var circles = this.svgContainer.selectAll("circle")
            .data(planets)
            .enter()
            .append("circle");
        var y = 50;
        var circleAttributes = circles
            .attr("cx", function (d) {
            var date = d.pl_publ_date ?
                new Date(d.pl_publ_date + '-01') :
                new Date(d.pl_disc + '-01-01');
            return _this._dateToXCoord(date);
        })
            .attr("cy", function (d) {
            return d.pl_bmasse;
        })
            .attr("r", function (d) { return d.pl_rade; })
            .style("opacity", .2)
            .style("stroke", "black")
            .style("fill", function (d) {
            var returnColor;
            if (d.st_spstr && d.st_spstr.indexOf('K4') !== -1) {
                returnColor = "green";
            }
            else if (d.st_spstr && d.st_spstr.indexOf('K0') !== -1) {
                returnColor = "purple";
            }
            else {
                returnColor = "red";
            }
            return returnColor;
        });
    };
    PlanetMapperComponent.prototype._dateToXCoord = function (date) {
        return (((date.getFullYear() - 1990) * 11) + date.getMonth());
    };
    PlanetMapperComponent = __decorate([
        core_1.Component({
            selector: 'planet-mapper',
            template: "<div id=\"planetMapperSvg\"></div>",
            providers: [planet_1.PlanetService, http_1.HTTP_PROVIDERS]
        }), 
        __metadata('design:paramtypes', [planet_1.PlanetService])
    ], PlanetMapperComponent);
    return PlanetMapperComponent;
}());
exports.PlanetMapperComponent = PlanetMapperComponent;
//# sourceMappingURL=planetMapper.js.map