import { OnInit, Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';

import * as d3 from 'd3';
import { PlanetService } from '../../services/planet/planet';
import { Planet } from '../../dto/planet/planet';
// import planetMapperMarkup from './planetMapper.html!text';



/* Note(s):
    - If an import begins with a dot, then it is treated as a relative path from the file that declares the import.
    - If an import begins with an absolute path, then it is assumed to be an external module, so Typescript will walk up the tree looking for a package.json file, then go into the node_modulesfolder, and find a folder with the same name as the import, then looks in the package.json of the module for the main .d.ts or .ts file, and then loads that, or will look for a file that has the same name as the one specified, or an index.d.ts or index.ts file.

 */
@Component({
  selector: 'planet-mapper',
  template: `<div id="planetMapperSvg"></div>`,
  providers: [ PlanetService, HTTP_PROVIDERS ]
})
export class PlanetMapperComponent implements OnInit {
    
    planets: Planet[] = [];
    diameter: number = 960;
    format = d3.format(",d");
    color: d3.scale.Ordinal<string, string> = d3.scale.category20c();
    svgContainer: d3.Selection<any>;

    constructor(private planetService : PlanetService) {


    
    }

    ngOnInit() {

        this.svgContainer = d3.select("#planetMapperSvg")
            .append("svg")
            .attr("width", 10000)
            .attr("height", 10000);

        console.log('PlanetMapperComponent.constructor...');
        this.planetService.getPlanets()
            .subscribe(planets => {
                this._populateSvgContainer(planets)
            });
    }

    _populateSvgContainer(planets: Planet[]) {
        console.log('planets: ', planets);
        // let planetData = planets.map(d => {
        //     return {
        //         x: (d.pl_disc - 1900),
        //         radius: d.pl_radj
        //     };
        // })

        let circles = this.svgContainer.selectAll("circle")
            .data(planets)
            .enter()
            .append("circle");

        let y = 50;
        let circleAttributes = circles
            .attr("cx", d => {
                // console.log('d: ', d);
                let date: Date = d.pl_publ_date ?
                        new Date(d.pl_publ_date + '-01') :
                        new Date(d.pl_disc + '-01-01');
                return this._dateToXCoord(date);

            })
            .attr("cy", function (d) {
                return d.pl_bmasse;
            })
            .attr("r", function(d) { return d.pl_rade; } )
            .style("opacity", .2)      // set the element opacity
            .style("stroke", "black")    // set the line colour
            .style("fill", function(d) {
                let returnColor;
                if (d.st_spstr && d.st_spstr.indexOf('K4') !== -1) {
                    returnColor = "green";
                } else if (d.st_spstr && d.st_spstr.indexOf('K0') !== -1) {
                    returnColor = "purple";
                } else {
                    returnColor = "red";
                }
                return returnColor;
            });
    }


    private _dateToXCoord(date: Date) {

        return (((date.getFullYear() - 1990) * 11) + date.getMonth());
    }


}